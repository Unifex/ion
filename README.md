# Inner Orion Network

ION is a community driven replacement for GalNet.

This project provides a potential system to manage the collection, editorial process and preparation of reports for the ION network.

## Installation

This is a [Drupal](https://drupal.org) site. Preparing your prefered development envirionment is beyond the scope of this document.

### Quick Start

Set up your development environment. Ensure you have configured your DB connection in your `web/sites/settings.local.php` file.

Stand up the site;

  drush site:install --existing-config

## Roadmap

* Initial Article gathering
* Workflow to ensure an Editor has reviewed an Article
* Reviewed Article selector
* View of Selected Articles for TTS
* Phonetic replacements
* Filter/format view of Selected Articles for TTS using phonetic replacements

### Longer term roadmap

* The ION app
  * Mobile is basically a podcast player. Play items added since you last checked.
  * Desktop app watches your ED Journal, reacts to jumps and plays articles of things 'nearby'.

## Need Help or Found a Bug?

Bugs can be reported on GitLab. Pull requests welcomed.